import {IShips} from "./IShips";
import {Field} from "./Field";

export class AI {
    private _listShips: any;
    private _playField: Field;

    //funkce co vygeneruje počítači lodě
    gnerateShipOnField(sizeFiled: number, ship: IShips, field: any): boolean {
        let generateNumberX: number = (Math.floor(Math.random() * (sizeFiled - 1))) + 1;
        let generateNumberY: number = (Math.floor(Math.random() * (sizeFiled - 1)) ) + 1;
        let coordString = "y_" + generateNumberX + "_" + generateNumberY;
        return ship.print(coordString, field);
    }
    //funkce která slouží k výstřelu
    shotShip(sizeFiled: number) {
        let generateNumberX: number = (Math.floor(Math.random() * (sizeFiled - 1))) + 1;
        let generateNumberY: number = (Math.floor(Math.random() * (sizeFiled - 1)) ) + 1;
        let coordString = "m_" + generateNumberX + "_" + generateNumberY;
        return coordString;
    }


    constructor() {
    }

    get listShips(): any {
        return this._listShips;
    }

    set listShips(value: any) {
        this._listShips = value;
    }

    get playField(): Field {
        return this._playField;
    }

    set playField(value: Field) {
        this._playField = value;
    }
}