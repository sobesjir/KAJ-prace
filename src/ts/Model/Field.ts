import {Coord} from "./Coord";
import {IShips} from "./IShips";


export class Field {
    private _size: number;
    private _arrayFiledValuesMy: Coord[][];
    private _arrayFiledValuesYour: Coord[][];

    constructor() {
        this._arrayFiledValuesMy = [];
        this._arrayFiledValuesYour = [];
    }

    get size(): number {
        return this._size;
    }

    set size(value: number) {
        this._size = value;
    }


    get arrayFiledValuesMy(): Coord[][] {
        return this._arrayFiledValuesMy;
    }

    set arrayFiledValuesMy(value: Coord[][]) {
        this._arrayFiledValuesMy = value;
    }

    get arrayFiledValuesYour(): Coord[][] {
        return this._arrayFiledValuesYour;
    }

    set arrayFiledValuesYour(value: Coord[][]) {
        this._arrayFiledValuesYour = value;
    }
    //funkce která vygeneruje pole
    generatedField(name: string) {
        // dodelat raster
        let neco = document.getElementById(name);
        let numb: number = parseInt((<HTMLSelectElement> document.getElementById("size")).value) + 1;
        // document.getElementById(name).innerText = "hueheueue";
        let array: Coord[][] = [];
        for (let i: number = 0; i < numb; i++) {

            array[i] = [];
            let gen = document.createElement("div");
            gen.setAttribute("class", "row");
            // gen.innerText = ("x");
            for (let j: number = 0; j < numb; j++) {
                array[i][j] = new Coord();
                array[i][j].x = i;
                array[i][j].y = j;


                let row = document.createElement("div");
                row.setAttribute("class", "column");
                row.setAttribute("id", name+  j + "_" + i);
                row.classList.add(name + j + "_" + i);


                array[i][j].name = name + j + "_" + i;
                array[i][j].value = 0;

                row.classList.add("oneField");

                if (j === 0) {
                    row.innerText = i + " ";
                }
                else if (i === 0) {
                    row.innerText = String.fromCharCode(96 + j) + " ";
                }
                else {
                    // row.innerText = "x ";
                }
                gen.appendChild(row);
            }

            let nn = $("p").hide();
            console.log(numb);
            neco.appendChild(gen);
        }
        if (name === "m_") {
            this._arrayFiledValuesMy = array;
        }
        else {
            this._arrayFiledValuesYour = array
        }
        console.log(this._arrayFiledValuesMy);
        console.log(this._arrayFiledValuesYour);

    }

}
