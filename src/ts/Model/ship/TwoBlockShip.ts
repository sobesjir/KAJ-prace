import {IShips} from "../IShips";
import {Field} from "../Field";

export class TwoBlockShip implements IShips {
    private _value: number = 3;

    print(coord: string, playField: any): any {
        let coordNumbers: any;
        coordNumbers = coord.split("_");
        let coordx = coordNumbers[2];
        let coordy = coordNumbers[1];
        console.log(coordNumbers);
        let x = parseInt(coordx);
        let y = parseInt(coordy);
        if (y + 1 < playField[x].length) {
            if (playField[y][x].value == 0
                && playField[y + 1][x].value == 0) {
                let point: any = document.getElementById(coord);
                if (coordNumbers[0] != 'y') {
                    // point.classList.add("darkolivegreen");
                    point.classList.add("blue");
                    point = document.getElementById(playField[x][y + 1].name);
                    // point.classList.add("darkolivegreen");
                    point.classList.add("blue");
                }
                playField[y][x].value = this._value;
                playField[y + 1][x].value = this._value;
                return true;
            }
        }
        return false;
    }

    getValue(): number {
        return this._value;
    }

}