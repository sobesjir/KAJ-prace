import {IShips} from "../IShips";
import {SquarteShip} from "./SquarteShip";
import {TriangleShip} from "./TriangleShip";
import {TwoBlockShip} from "./TwoBlockShip";
import {ThreeBlockShip} from "./ThreeBlockShip";
//třída pro vytváření lodí na základě indexu
export class FactoryShip {

    static createShip(number: number): IShips {
        if (number == 1) {
             return new SquarteShip();
        }
        if (number == 2) {
            return new TriangleShip();
        }
        if (number == 3) {
            return new TwoBlockShip();
        }
        else {
            return new ThreeBlockShip();
        }
    }
}