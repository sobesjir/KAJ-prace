import {IShips} from "../IShips";

export class SquarteShip implements IShips {

    private _value = 1;

    get value(): number {
        return this._value;
    }

    set value(value: number) {
        this._value = value;
    }

    constructor() {
    }

    getValue(): number {
        return this._value;
    }

    print(coord: string, playField: any): any {
        let coordNumbers: any;
        coordNumbers = coord.split("_");
        let coordx = coordNumbers[2];
        let coordy = coordNumbers[1];
        console.log(coordNumbers);
        let x = parseInt(coordx);
        let y = parseInt(coordy);
        if (playField[y][x].value === 0) {
            let point: any = document.getElementById(coord);
            playField[y][x].value = this._value;
            if (coordNumbers[0] != "y") {
                // point.classList.add("darkseagreen");
                point.classList.add("blue");
            }
            return true;
        }
        return false;
    }
}