// třída která reprezentuje jeden bod v poli
export class Coord{
    private _x:number;
    private _y:number;
    private _name:string;
    private _value:number;

    constructor(){

    }
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get value(): number {
        return this._value;
    }

    set value(value: number) {
        this._value = value;
    }

    get x(): number {
        return this._x;
    }

    set x(value: number) {
        this._x = value;
    }

    get y(): number {
        return this._y;
    }

    set y(value: number) {
        this._y = value;
    }
}