
import {Field} from "./Model/Field";
import {IShips} from "./Model/IShips";
import {SquarteShip} from "./Model/ship/SquarteShip";
import {TriangleShip} from "./Model/ship/TriangleShip";
import {ThreeBlockShip} from "./Model/ship/ThreeBlockShip";
import {TwoBlockShip} from "./Model/ship/TwoBlockShip";
import {AI} from "./Model/AI";
import {FactoryShip} from "./Model/ship/FactoryShip";

var ship: IShips;
var playField: Field;
var playerAI: AI;
var countSquareShip = 5;
var countTripleShip = 3;
var countTiangleShip = 3;
var countDoubleShip = 2;
var listShips: any = [{value: 1, count: 5}, {value: 2, count: 3}, {value: 3, count: 2}, {value: 4, count: 3}];
var date = new Date();
playField = new Field();
playerAI = new AI();
main("greeting");
function main(divName: string) {
    document.getElementById("smileLose").style.display = "none";
    document.getElementById("smileWin").style.display = "none";
    let elt = document.getElementById(divName);
    let field = createField(playField);
    elt.appendChild(field);
    createCountersShip();
}

//vytvoří hráčovo pole a počítače
function createField(playField: Field) {
    let result = document.createElement('p');
    result.innerText = ("vlozte pocet poli : ");
    let sizeField = document.createElement('input');
    sizeField.setAttribute("type", "number");
    sizeField.setAttribute("id", "size");
    sizeField.setAttribute("min", "10");
    sizeField.setAttribute("message", "rozmezi od 10 do 25");
    sizeField.setAttribute("max", "25");

    let buttonSizeField = document.createElement('button');
    buttonSizeField.innerText = ('generate');
    buttonSizeField.addEventListener("click", () => {
        if ((<HTMLSelectElement>document.getElementById("size")).value <= "25") {
            playField.generatedField("m_");
            playField.generatedField("y_");
        }

    });
    result.appendChild(sizeField);
    result.appendChild(buttonSizeField);
    return result;
}

//funkce která jen pro každou buňku v poly přidá daný posluchač
function addListenerShip(playField: Field, funcion: any) {
    for (let i = 1; i < playField.arrayFiledValuesMy.length; i++) {
        for (let j = 1; j < playField.arrayFiledValuesMy[i].length; j++) {
            let item = document.getElementById(playField.arrayFiledValuesMy[i][j].name);
            item.addEventListener("click", funcion)
        }
    }
}
//smaže posluchače z pole
function removeListenerShip(playField: Field, funcion: any) {
    for (let i = 1; i < playField.arrayFiledValuesMy.length; i++) {
        for (let j = 1; j < playField.arrayFiledValuesMy[i].length; j++) {
            let item = playField.arrayFiledValuesMy[i][j].name;
            document.getElementById(item).removeEventListener("click", funcion);
            // console.count("hit");
        }
    }
}
//funkce která počítá počet lodí které zbívají položit z výběru všech lodí
function createCountersShip() {
    let elementSquareShip = document.createElement("div");
    let btn = document.getElementById("squareShip");
    btn.addEventListener("click", e => {
        let oldnumber = elementSquareShip;
        elementSquareShip.innerText = "" + countSquareShip;
        if (document.getElementById("btnShip-squareShip").lastChild.nodeName == "DIV") {
            document.getElementById("btnShip-squareShip").lastChild.replaceChild(elementSquareShip.childNodes[0], oldnumber.childNodes[0]);
        }
        document.getElementById("btnShip-squareShip").appendChild(elementSquareShip);
        ship = new SquarteShip();
        removeListenerShip(playField, coloredBox);
        addListenerShip(playField, coloredBox);
    });

    let elementTiangleShip = document.createElement("div");
    let btn1 = document.getElementById("tiangleShip");
    btn1.addEventListener("click", e => {
        let oldnumber = elementTiangleShip;
        elementTiangleShip.innerText = "" + countTiangleShip;
        if (document.getElementById("btnShip-tiangleShip").lastChild.nodeName == "DIV") {
            document.getElementById("btnShip-tiangleShip").lastChild.replaceChild(elementTiangleShip.childNodes[0], oldnumber.childNodes[0]);
        }
        document.getElementById("btnShip-tiangleShip").appendChild(elementTiangleShip);
        ship = new TriangleShip();
        removeListenerShip(playField, coloredBox);
        addListenerShip(playField, coloredBox);
    });

    let elementTripleShip = document.createElement("div");
    let btn2 = document.getElementById("tripleShip");
    btn2.addEventListener("click", e => {
        let oldnumber = elementTripleShip;
        elementTripleShip.innerText = "" + countTripleShip;
        if (document.getElementById("btnShip-tripleShip").lastChild.nodeName == "DIV") {
            document.getElementById("btnShip-tripleShip").lastChild.replaceChild(elementTripleShip.childNodes[0], oldnumber.childNodes[0]);
        }
        document.getElementById("btnShip-tripleShip").appendChild(elementTripleShip);
        ship = new ThreeBlockShip();
        removeListenerShip(playField, coloredBox);
        addListenerShip(playField, coloredBox);
    });

    let elementDoubleShip = document.createElement("div");
    let btn3 = document.getElementById("doubleShip");
    btn3.addEventListener("click", e => {
        let oldnumber = elementDoubleShip;
        elementDoubleShip.innerText = "" + countDoubleShip;
        if (document.getElementById("btnShip-doubleShip").lastChild.nodeName == "DIV") {
            document.getElementById("btnShip-doubleShip").lastChild.replaceChild(elementDoubleShip.childNodes[0], oldnumber.childNodes[0]);
        }
        document.getElementById("btnShip-doubleShip").appendChild(elementDoubleShip);
        ship = new TwoBlockShip();
        removeListenerShip(playField, coloredBox);
        addListenerShip(playField, coloredBox);
    });
}
//funkce která obarvuje lodě a odečítá lodě pokud se položí do pole
function coloredBox(e: MouseEvent) {
    if (ship != null) {
        let bol = ship.print(e.toElement.id, playField.arrayFiledValuesMy);
        if (bol && ship.getValue() == 1) {
            countSquareShip--;
        }
        else if (bol && ship.getValue() == 2) {
            countTiangleShip--;
        }
        else if (bol && ship.getValue() == 3) {
            countDoubleShip--;
        }
        else if (bol && ship.getValue() == 4) {
            countTripleShip--;
        }
        if (countSquareShip <= 0) {
            document.getElementById("btnShip-squareShip").style.visibility = 'hidden';
        }
        if (countTiangleShip <= 0) {
            document.getElementById("btnShip-tiangleShip").style.visibility = 'hidden';
        }
        if (countDoubleShip <= 0) {
            document.getElementById("btnShip-doubleShip").style.visibility = 'hidden';
        }
        if (countTripleShip <= 0) {
            document.getElementById("btnShip-tripleShip").style.visibility = 'hidden';
        }
        ship = null;

        if (countSquareShip == 0 && countDoubleShip == 0 && countTiangleShip == 0 && countTripleShip == 0) {
            for (let i = 0; i < listShips.length; i++) {
                let idShip = listShips[i].value;
                let shipAI: IShips = FactoryShip.createShip(idShip);
                let count = listShips[i].count;
                while (count != 0) {
                    if (playerAI.gnerateShipOnField(playField.arrayFiledValuesYour.length, shipAI, playField.arrayFiledValuesYour)) {
                        count--;
                    }
                }
            }
            console.log("ready to play");
            let root = document.getElementById("fieldShips");
            let startTestButtom = document.createElement("div");
            startTestButtom.innerText = "Start";
            startTestButtom.setAttribute("class", "btn");
            root.appendChild(startTestButtom);
            removeListenerShip(playField, shotShip);
            addListenerShipForShoting(playField, shotShip);
        }
    }

}
//posluchač pro střílení do lodí
function addListenerShipForShoting(playField: Field, funcion: any) {
    for (let i = 1; i < playField.arrayFiledValuesYour.length; i++) {
        for (let j = 1; j < playField.arrayFiledValuesYour[i].length; j++) {
            let item = document.getElementById(playField.arrayFiledValuesYour[i][j].name);
            item.addEventListener("click", funcion)
        }
    }
}
//test jestli vyhrál hráč
function checkEndHuman(playField: Field): boolean {
    let check = true;
    for (let i = 1; i < playField.arrayFiledValuesYour.length; i++) {
        for (let j = 1; j < playField.arrayFiledValuesYour[i].length; j++) {
            if (playField.arrayFiledValuesYour[i][j].value > 0) {
                check = false;
            }
        }
    }
    return check;
}
//test jestli vyhrál počítač
function checkEndAI(playField: Field): boolean {
    let check = true;
    for (let i = 1; i < playField.arrayFiledValuesMy.length; i++) {
        for (let j = 1; j < playField.arrayFiledValuesMy[i].length; j++) {
            if (playField.arrayFiledValuesMy[i][j].value > 0) {
                check = false;
            }
        }
    }
    return check;
}
//funkce která se stará o samostatnou střelbu na lodě
function shotShip(e: MouseEvent) {
    date = new Date();
    var modal = document.getElementById('myModal');
    let span = <HTMLElement> document.getElementsByClassName("close")[0];
    span.onclick = function () {
        modal.style.display = "none";
    };
    var snd = new Audio("hit.wav"); // buffers automatically when created
    let coordName: any;
    coordName = e.toElement.id.split("_");
    let coordx = coordName[2];
    let coordy = coordName[1];
    if (playField.arrayFiledValuesYour[coordy][coordx].value == 0) {
        e.toElement.classList.add("miss");
    }
    else if (playField.arrayFiledValuesYour[coordy][coordx].value > 0) {
        snd.play();
        e.toElement.classList.add("hit");
        playField.arrayFiledValuesYour[coordy][coordx].value = -1;
    }

    let coordAI: any;
    let string: any;
    let temp = true;
    //end game


    while (temp) {
        string = playerAI.shotShip(playField.arrayFiledValuesMy.length);
        if (document.getElementById(string).classList.contains("miss") || document.getElementById(string).classList.contains("hit")) {
            // string = playerAI.shotShip(playField.arrayFiledValuesMy.length);
        }
        else {
            temp = false;
        }
    }
    coordAI = string.split("_");
    let coordAI_X = coordAI[2];
    let coordAI_y = coordAI[1];

    if (playField.arrayFiledValuesMy[coordAI_y][coordAI_X].value == 0) {
        document.getElementById(string).classList.add("miss");
    }
    else if (playField.arrayFiledValuesMy[coordAI_y][coordAI_X].value > 0) {
        snd.play();
        document.getElementById(string).classList.add("hit");
        playField.arrayFiledValuesMy[coordAI_y][coordAI_X].value = -1;
    }


    if (checkEndHuman(playField)) {
        document.getElementById("smileWin").style.display = "block";
        status = "WIN";
        console.log("END GAME YOU WIN");
        document.getElementById("text").innerText = "You win";
        foreachLocalStorage();
        modal.style.display = "block";
    }
    else if (checkEndAI(playField)) {
        document.getElementById("smileLose").style.display = "block";
        status = "LOSE";
        console.log("END GAME YOU LOSE");
        document.getElementById("text").innerText = "You lose";
        foreachLocalStorage();
        modal.style.display = "block";
    }

}
//funkce která vypisuje celí localstorage
function foreachLocalStorage() {
    for (var key in localStorage) {
        var newInput = document.createElement("div");
        newInput.innerText = localStorage.getItem(key);
        document.getElementById("history").appendChild(newInput);
    }
}