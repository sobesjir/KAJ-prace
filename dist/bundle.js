(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });

var AI = function () {
    _createClass(AI, [{
        key: "gnerateShipOnField",

        //funkce co vygeneruje počítači lodě
        value: function gnerateShipOnField(sizeFiled, ship, field) {
            var generateNumberX = Math.floor(Math.random() * (sizeFiled - 1)) + 1;
            var generateNumberY = Math.floor(Math.random() * (sizeFiled - 1)) + 1;
            var coordString = "y_" + generateNumberX + "_" + generateNumberY;
            return ship.print(coordString, field);
        }
        //funkce která slouží k výstřelu

    }, {
        key: "shotShip",
        value: function shotShip(sizeFiled) {
            var generateNumberX = Math.floor(Math.random() * (sizeFiled - 1)) + 1;
            var generateNumberY = Math.floor(Math.random() * (sizeFiled - 1)) + 1;
            var coordString = "m_" + generateNumberX + "_" + generateNumberY;
            return coordString;
        }
    }]);

    function AI() {
        _classCallCheck(this, AI);
    }

    _createClass(AI, [{
        key: "listShips",
        get: function get() {
            return this._listShips;
        },
        set: function set(value) {
            this._listShips = value;
        }
    }, {
        key: "playField",
        get: function get() {
            return this._playField;
        },
        set: function set(value) {
            this._playField = value;
        }
    }]);

    return AI;
}();

exports.AI = AI;

},{}],2:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });
// třída která reprezentuje jeden bod v poli

var Coord = function () {
    function Coord() {
        _classCallCheck(this, Coord);
    }

    _createClass(Coord, [{
        key: "name",
        get: function get() {
            return this._name;
        },
        set: function set(value) {
            this._name = value;
        }
    }, {
        key: "value",
        get: function get() {
            return this._value;
        },
        set: function set(value) {
            this._value = value;
        }
    }, {
        key: "x",
        get: function get() {
            return this._x;
        },
        set: function set(value) {
            this._x = value;
        }
    }, {
        key: "y",
        get: function get() {
            return this._y;
        },
        set: function set(value) {
            this._y = value;
        }
    }]);

    return Coord;
}();

exports.Coord = Coord;

},{}],3:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });
var Coord_1 = require("./Coord");

var Field = function () {
    function Field() {
        _classCallCheck(this, Field);

        this._arrayFiledValuesMy = [];
        this._arrayFiledValuesYour = [];
    }

    _createClass(Field, [{
        key: "generatedField",

        //funkce která vygeneruje pole
        value: function generatedField(name) {
            // dodelat raster
            var neco = document.getElementById(name);
            var numb = parseInt(document.getElementById("size").value) + 1;
            // document.getElementById(name).innerText = "hueheueue";
            var array = [];
            for (var i = 0; i < numb; i++) {
                array[i] = [];
                var gen = document.createElement("div");
                gen.setAttribute("class", "row");
                // gen.innerText = ("x");
                for (var j = 0; j < numb; j++) {
                    array[i][j] = new Coord_1.Coord();
                    array[i][j].x = i;
                    array[i][j].y = j;
                    var row = document.createElement("div");
                    row.setAttribute("class", "column");
                    row.setAttribute("id", name + j + "_" + i);
                    row.classList.add(name + j + "_" + i);
                    array[i][j].name = name + j + "_" + i;
                    array[i][j].value = 0;
                    row.classList.add("oneField");
                    if (j === 0) {
                        row.innerText = i + " ";
                    } else if (i === 0) {
                        row.innerText = String.fromCharCode(96 + j) + " ";
                    } else {
                        // row.innerText = "x ";
                    }
                    gen.appendChild(row);
                }
                var nn = $("p").hide();
                console.log(numb);
                neco.appendChild(gen);
            }
            if (name === "m_") {
                this._arrayFiledValuesMy = array;
            } else {
                this._arrayFiledValuesYour = array;
            }
            console.log(this._arrayFiledValuesMy);
            console.log(this._arrayFiledValuesYour);
        }
    }, {
        key: "size",
        get: function get() {
            return this._size;
        },
        set: function set(value) {
            this._size = value;
        }
    }, {
        key: "arrayFiledValuesMy",
        get: function get() {
            return this._arrayFiledValuesMy;
        },
        set: function set(value) {
            this._arrayFiledValuesMy = value;
        }
    }, {
        key: "arrayFiledValuesYour",
        get: function get() {
            return this._arrayFiledValuesYour;
        },
        set: function set(value) {
            this._arrayFiledValuesYour = value;
        }
    }]);

    return Field;
}();

exports.Field = Field;

},{"./Coord":2}],4:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });
var SquarteShip_1 = require("./SquarteShip");
var TriangleShip_1 = require("./TriangleShip");
var TwoBlockShip_1 = require("./TwoBlockShip");
var ThreeBlockShip_1 = require("./ThreeBlockShip");
//třída pro vytváření lodí na základě indexu

var FactoryShip = function () {
    function FactoryShip() {
        _classCallCheck(this, FactoryShip);
    }

    _createClass(FactoryShip, null, [{
        key: "createShip",
        value: function createShip(number) {
            if (number == 1) {
                return new SquarteShip_1.SquarteShip();
            }
            if (number == 2) {
                return new TriangleShip_1.TriangleShip();
            }
            if (number == 3) {
                return new TwoBlockShip_1.TwoBlockShip();
            } else {
                return new ThreeBlockShip_1.ThreeBlockShip();
            }
        }
    }]);

    return FactoryShip;
}();

exports.FactoryShip = FactoryShip;

},{"./SquarteShip":5,"./ThreeBlockShip":6,"./TriangleShip":7,"./TwoBlockShip":8}],5:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });

var SquarteShip = function () {
    function SquarteShip() {
        _classCallCheck(this, SquarteShip);

        this._value = 1;
    }

    _createClass(SquarteShip, [{
        key: "getValue",
        value: function getValue() {
            return this._value;
        }
    }, {
        key: "print",
        value: function print(coord, playField) {
            var coordNumbers = void 0;
            coordNumbers = coord.split("_");
            var coordx = coordNumbers[2];
            var coordy = coordNumbers[1];
            console.log(coordNumbers);
            var x = parseInt(coordx);
            var y = parseInt(coordy);
            if (playField[y][x].value === 0) {
                var point = document.getElementById(coord);
                playField[y][x].value = this._value;
                if (coordNumbers[0] != "y") {
                    // point.classList.add("darkseagreen");
                    point.classList.add("blue");
                }
                return true;
            }
            return false;
        }
    }, {
        key: "value",
        get: function get() {
            return this._value;
        },
        set: function set(value) {
            this._value = value;
        }
    }]);

    return SquarteShip;
}();

exports.SquarteShip = SquarteShip;

},{}],6:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });

var ThreeBlockShip = function () {
    function ThreeBlockShip() {
        _classCallCheck(this, ThreeBlockShip);

        this._value = 4;
    }
    //todo prepsat vsechny modeli kvuli IShip


    _createClass(ThreeBlockShip, [{
        key: "print",
        value: function print(coord, playField) {
            var coordNumbers = void 0;
            coordNumbers = coord.split("_");
            var coordx = coordNumbers[2];
            var coordy = coordNumbers[1];
            console.log(coordNumbers);
            var x = parseInt(coordx);
            var y = parseInt(coordy);
            if (y + 1 < playField[x].length && y - 1 >= 1) {
                if (playField[y][x].value == 0 && playField[y - 1][x].value == 0 && playField[y + 1][x].value == 0) {
                    var point = document.getElementById(coord);
                    if (coordNumbers[0] != "y") {
                        // point.classList.add("chartreuse");
                        point.classList.add("blue");
                        point = document.getElementById(playField[x][y + 1].name);
                        // point.classList.add("chartreuse");
                        point.classList.add("blue");
                        point = document.getElementById(playField[x][y - 1].name);
                        // point.classList.add("chartreuse");
                        point.classList.add("blue");
                    }
                    playField[y][x].value = this._value;
                    playField[y - 1][x].value = this._value;
                    playField[y + 1][x].value = this._value;
                    return true;
                }
            }
            return false;
        }
    }, {
        key: "getValue",
        value: function getValue() {
            return this._value;
        }
    }]);

    return ThreeBlockShip;
}();

exports.ThreeBlockShip = ThreeBlockShip;

},{}],7:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });

var TriangleShip = function () {
    function TriangleShip() {
        _classCallCheck(this, TriangleShip);

        this._value = 2;
    }

    _createClass(TriangleShip, [{
        key: "print",
        value: function print(coord, playField) {
            var coordNumbers = void 0;
            coordNumbers = coord.split("_");
            var coordx = coordNumbers[2];
            var coordy = coordNumbers[1];
            console.log(coordNumbers);
            var x = parseInt(coordx);
            var y = parseInt(coordy);
            if (y + 1 < playField[x].length && y - 1 >= 1 && playField.length > x && x - 1 >= 1) {
                if (playField[y][x].value == 0 && playField[y - 1][x].value == 0 && playField[y][x - 1].value == 0 && playField[y + 1][x].value == 0) {
                    var point = document.getElementById(coord);
                    if (coordNumbers[0] != "y") {
                        point.classList.add("blue");
                        point = document.getElementById(playField[x][y + 1].name);
                        point.classList.add("blue");
                        point = document.getElementById(playField[x][y - 1].name);
                        point.classList.add("blue");
                        point = document.getElementById(playField[x - 1][y].name);
                        point.classList.add("blue");
                    }
                    playField[y][x].value = this._value;
                    playField[y - 1][x].value = this._value;
                    playField[y][x - 1].value = this._value;
                    playField[y + 1][x].value = this._value;
                    return true;
                }
            }
            return false;
        }
    }, {
        key: "getValue",
        value: function getValue() {
            return this._value;
        }
    }]);

    return TriangleShip;
}();

exports.TriangleShip = TriangleShip;

},{}],8:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });

var TwoBlockShip = function () {
    function TwoBlockShip() {
        _classCallCheck(this, TwoBlockShip);

        this._value = 3;
    }

    _createClass(TwoBlockShip, [{
        key: "print",
        value: function print(coord, playField) {
            var coordNumbers = void 0;
            coordNumbers = coord.split("_");
            var coordx = coordNumbers[2];
            var coordy = coordNumbers[1];
            console.log(coordNumbers);
            var x = parseInt(coordx);
            var y = parseInt(coordy);
            if (y + 1 < playField[x].length) {
                if (playField[y][x].value == 0 && playField[y + 1][x].value == 0) {
                    var point = document.getElementById(coord);
                    if (coordNumbers[0] != 'y') {
                        // point.classList.add("darkolivegreen");
                        point.classList.add("blue");
                        point = document.getElementById(playField[x][y + 1].name);
                        // point.classList.add("darkolivegreen");
                        point.classList.add("blue");
                    }
                    playField[y][x].value = this._value;
                    playField[y + 1][x].value = this._value;
                    return true;
                }
            }
            return false;
        }
    }, {
        key: "getValue",
        value: function getValue() {
            return this._value;
        }
    }]);

    return TwoBlockShip;
}();

exports.TwoBlockShip = TwoBlockShip;

},{}],9:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Field_1 = require("./Model/Field");
var SquarteShip_1 = require("./Model/ship/SquarteShip");
var TriangleShip_1 = require("./Model/ship/TriangleShip");
var ThreeBlockShip_1 = require("./Model/ship/ThreeBlockShip");
var TwoBlockShip_1 = require("./Model/ship/TwoBlockShip");
var AI_1 = require("./Model/AI");
var FactoryShip_1 = require("./Model/ship/FactoryShip");
var ship;
var playField;
var playerAI;
var countSquareShip = 5;
var countTripleShip = 3;
var countTiangleShip = 3;
var countDoubleShip = 2;
var listShips = [{ value: 1, count: 5 }, { value: 2, count: 3 }, { value: 3, count: 2 }, { value: 4, count: 3 }];
var date = new Date();
playField = new Field_1.Field();
playerAI = new AI_1.AI();
main("greeting");
function main(divName) {
    document.getElementById("smileLose").style.display = "none";
    document.getElementById("smileWin").style.display = "none";
    var elt = document.getElementById(divName);
    var field = createField(playField);
    elt.appendChild(field);
    createCountersShip();
}
//vytvoří hráčovo pole a počítače
function createField(playField) {
    var result = document.createElement('p');
    result.innerText = "vlozte pocet poli : ";
    var sizeField = document.createElement('input');
    sizeField.setAttribute("type", "number");
    sizeField.setAttribute("id", "size");
    sizeField.setAttribute("min", "10");
    sizeField.setAttribute("message", "rozmezi od 10 do 25");
    sizeField.setAttribute("max", "25");
    var buttonSizeField = document.createElement('button');
    buttonSizeField.innerText = 'generate';
    buttonSizeField.addEventListener("click", function () {
        if (document.getElementById("size").value <= "25") {
            playField.generatedField("m_");
            playField.generatedField("y_");
        }
    });
    result.appendChild(sizeField);
    result.appendChild(buttonSizeField);
    return result;
}
//funkce která jen pro každou buňku v poly přidá daný posluchač
function addListenerShip(playField, funcion) {
    for (var i = 1; i < playField.arrayFiledValuesMy.length; i++) {
        for (var j = 1; j < playField.arrayFiledValuesMy[i].length; j++) {
            var item = document.getElementById(playField.arrayFiledValuesMy[i][j].name);
            item.addEventListener("click", funcion);
        }
    }
}
//smaže posluchače z pole
function removeListenerShip(playField, funcion) {
    for (var i = 1; i < playField.arrayFiledValuesMy.length; i++) {
        for (var j = 1; j < playField.arrayFiledValuesMy[i].length; j++) {
            var item = playField.arrayFiledValuesMy[i][j].name;
            document.getElementById(item).removeEventListener("click", funcion);
            // console.count("hit");
        }
    }
}
//funkce která počítá počet lodí které zbívají položit z výběru všech lodí
function createCountersShip() {
    var elementSquareShip = document.createElement("div");
    var btn = document.getElementById("squareShip");
    btn.addEventListener("click", function (e) {
        var oldnumber = elementSquareShip;
        elementSquareShip.innerText = "" + countSquareShip;
        if (document.getElementById("btnShip-squareShip").lastChild.nodeName == "DIV") {
            document.getElementById("btnShip-squareShip").lastChild.replaceChild(elementSquareShip.childNodes[0], oldnumber.childNodes[0]);
        }
        document.getElementById("btnShip-squareShip").appendChild(elementSquareShip);
        ship = new SquarteShip_1.SquarteShip();
        removeListenerShip(playField, coloredBox);
        addListenerShip(playField, coloredBox);
    });
    var elementTiangleShip = document.createElement("div");
    var btn1 = document.getElementById("tiangleShip");
    btn1.addEventListener("click", function (e) {
        var oldnumber = elementTiangleShip;
        elementTiangleShip.innerText = "" + countTiangleShip;
        if (document.getElementById("btnShip-tiangleShip").lastChild.nodeName == "DIV") {
            document.getElementById("btnShip-tiangleShip").lastChild.replaceChild(elementTiangleShip.childNodes[0], oldnumber.childNodes[0]);
        }
        document.getElementById("btnShip-tiangleShip").appendChild(elementTiangleShip);
        ship = new TriangleShip_1.TriangleShip();
        removeListenerShip(playField, coloredBox);
        addListenerShip(playField, coloredBox);
    });
    var elementTripleShip = document.createElement("div");
    var btn2 = document.getElementById("tripleShip");
    btn2.addEventListener("click", function (e) {
        var oldnumber = elementTripleShip;
        elementTripleShip.innerText = "" + countTripleShip;
        if (document.getElementById("btnShip-tripleShip").lastChild.nodeName == "DIV") {
            document.getElementById("btnShip-tripleShip").lastChild.replaceChild(elementTripleShip.childNodes[0], oldnumber.childNodes[0]);
        }
        document.getElementById("btnShip-tripleShip").appendChild(elementTripleShip);
        ship = new ThreeBlockShip_1.ThreeBlockShip();
        removeListenerShip(playField, coloredBox);
        addListenerShip(playField, coloredBox);
    });
    var elementDoubleShip = document.createElement("div");
    var btn3 = document.getElementById("doubleShip");
    btn3.addEventListener("click", function (e) {
        var oldnumber = elementDoubleShip;
        elementDoubleShip.innerText = "" + countDoubleShip;
        if (document.getElementById("btnShip-doubleShip").lastChild.nodeName == "DIV") {
            document.getElementById("btnShip-doubleShip").lastChild.replaceChild(elementDoubleShip.childNodes[0], oldnumber.childNodes[0]);
        }
        document.getElementById("btnShip-doubleShip").appendChild(elementDoubleShip);
        ship = new TwoBlockShip_1.TwoBlockShip();
        removeListenerShip(playField, coloredBox);
        addListenerShip(playField, coloredBox);
    });
}
//funkce která obarvuje lodě a odečítá lodě pokud se položí do pole
function coloredBox(e) {
    if (ship != null) {
        var bol = ship.print(e.toElement.id, playField.arrayFiledValuesMy);
        if (bol && ship.getValue() == 1) {
            countSquareShip--;
        } else if (bol && ship.getValue() == 2) {
            countTiangleShip--;
        } else if (bol && ship.getValue() == 3) {
            countDoubleShip--;
        } else if (bol && ship.getValue() == 4) {
            countTripleShip--;
        }
        if (countSquareShip <= 0) {
            document.getElementById("btnShip-squareShip").style.visibility = 'hidden';
        }
        if (countTiangleShip <= 0) {
            document.getElementById("btnShip-tiangleShip").style.visibility = 'hidden';
        }
        if (countDoubleShip <= 0) {
            document.getElementById("btnShip-doubleShip").style.visibility = 'hidden';
        }
        if (countTripleShip <= 0) {
            document.getElementById("btnShip-tripleShip").style.visibility = 'hidden';
        }
        ship = null;
        if (countSquareShip == 0 && countDoubleShip == 0 && countTiangleShip == 0 && countTripleShip == 0) {
            for (var i = 0; i < listShips.length; i++) {
                var idShip = listShips[i].value;
                var shipAI = FactoryShip_1.FactoryShip.createShip(idShip);
                var count = listShips[i].count;
                while (count != 0) {
                    if (playerAI.gnerateShipOnField(playField.arrayFiledValuesYour.length, shipAI, playField.arrayFiledValuesYour)) {
                        count--;
                    }
                }
            }
            console.log("ready to play");
            var root = document.getElementById("fieldShips");
            var startTestButtom = document.createElement("div");
            startTestButtom.innerText = "Start";
            startTestButtom.setAttribute("class", "btn");
            root.appendChild(startTestButtom);
            removeListenerShip(playField, shotShip);
            addListenerShipForShoting(playField, shotShip);
        }
    }
}
//posluchač pro střílení do lodí
function addListenerShipForShoting(playField, funcion) {
    for (var i = 1; i < playField.arrayFiledValuesYour.length; i++) {
        for (var j = 1; j < playField.arrayFiledValuesYour[i].length; j++) {
            var item = document.getElementById(playField.arrayFiledValuesYour[i][j].name);
            item.addEventListener("click", funcion);
        }
    }
}
//test jestli vyhrál hráč
function checkEndHuman(playField) {
    var check = true;
    for (var i = 1; i < playField.arrayFiledValuesYour.length; i++) {
        for (var j = 1; j < playField.arrayFiledValuesYour[i].length; j++) {
            if (playField.arrayFiledValuesYour[i][j].value > 0) {
                check = false;
            }
        }
    }
    return check;
}
//test jestli vyhrál počítač
function checkEndAI(playField) {
    var check = true;
    for (var i = 1; i < playField.arrayFiledValuesMy.length; i++) {
        for (var j = 1; j < playField.arrayFiledValuesMy[i].length; j++) {
            if (playField.arrayFiledValuesMy[i][j].value > 0) {
                check = false;
            }
        }
    }
    return check;
}
//funkce která se stará o samostatnou střelbu na lodě
function shotShip(e) {
    date = new Date();
    var modal = document.getElementById('myModal');
    var span = document.getElementsByClassName("close")[0];
    span.onclick = function () {
        modal.style.display = "none";
    };
    var snd = new Audio("hit.wav"); // buffers automatically when created
    var coordName = void 0;
    coordName = e.toElement.id.split("_");
    var coordx = coordName[2];
    var coordy = coordName[1];
    if (playField.arrayFiledValuesYour[coordy][coordx].value == 0) {
        e.toElement.classList.add("miss");
    } else if (playField.arrayFiledValuesYour[coordy][coordx].value > 0) {
        snd.play();
        e.toElement.classList.add("hit");
        playField.arrayFiledValuesYour[coordy][coordx].value = -1;
    }
    var coordAI = void 0;
    var string = void 0;
    var temp = true;
    //end game
    while (temp) {
        string = playerAI.shotShip(playField.arrayFiledValuesMy.length);
        if (document.getElementById(string).classList.contains("miss") || document.getElementById(string).classList.contains("hit")) {
            // string = playerAI.shotShip(playField.arrayFiledValuesMy.length);
        } else {
            temp = false;
        }
    }
    coordAI = string.split("_");
    var coordAI_X = coordAI[2];
    var coordAI_y = coordAI[1];
    if (playField.arrayFiledValuesMy[coordAI_y][coordAI_X].value == 0) {
        document.getElementById(string).classList.add("miss");
    } else if (playField.arrayFiledValuesMy[coordAI_y][coordAI_X].value > 0) {
        snd.play();
        document.getElementById(string).classList.add("hit");
        playField.arrayFiledValuesMy[coordAI_y][coordAI_X].value = -1;
    }
    if (checkEndHuman(playField)) {
        document.getElementById("smileWin").style.display = "block";
        status = "WIN";
        console.log("END GAME YOU WIN");
        document.getElementById("text").innerText = "You win";
        foreachLocalStorage();
        modal.style.display = "block";
    } else if (checkEndAI(playField)) {
        document.getElementById("smileLose").style.display = "block";
        status = "LOSE";
        console.log("END GAME YOU LOSE");
        document.getElementById("text").innerText = "You lose";
        foreachLocalStorage();
        modal.style.display = "block";
    }
}
//funkce která vypisuje celí localstorage
function foreachLocalStorage() {
    for (var key in localStorage) {
        var newInput = document.createElement("div");
        newInput.innerText = localStorage.getItem(key);
        document.getElementById("history").appendChild(newInput);
    }
}

},{"./Model/AI":1,"./Model/Field":3,"./Model/ship/FactoryShip":4,"./Model/ship/SquarteShip":5,"./Model/ship/ThreeBlockShip":6,"./Model/ship/TriangleShip":7,"./Model/ship/TwoBlockShip":8}]},{},[9])

//# sourceMappingURL=bundle.js.map
